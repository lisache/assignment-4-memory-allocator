#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include <sys/mman.h>

#define HEAP_SIZE 10000

void* test_heap_init() {
	fprintf(stdout, "Heap init\n");
	void *heap = heap_init(HEAP_SIZE);
	if (heap == NULL) {
		fprintf(stderr, "Initialization failed\n");
		return NULL;
	}
	
	return heap;
}


int check(){
    if (test_heap_init() != NULL){
        int tests_passed = test1() + test2() + test3() + test4() + test5();
        return tests_passed;
    }
    return 0;

}
