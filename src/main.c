#include "tests.h"

int main() {
    int tests = check();
    if (tests) {
        fprintf(stdout, "%d %s %d %s", tests,  "tests passed from ", TESTS_NUM, "\n" );
    }
    else{
        fprintf(stderr, "%s",  "tests failed\n");
        return -1;
    }
    return 0;
}
