#include "../tests.h"

//Освобождение одного блока из нескольких выделенных.
int test2(){
    fprintf(stdout, "Test №2 \n");
	void* block1 = _malloc(SIZE);
    void* block2 = _malloc(SIZE);
    void* block3 = _malloc(SIZE);
    if (block1 == NULL || block2 == NULL || block3 == NULL){
        fprintf(stderr, "test 2 failed \n");
        return 0;
    }

	debug_heap(stdout, HEAP_START);
	_free(block2);
	debug_heap(stdout, HEAP_START);
	struct block_header *block1_header = block_get_header(block1);
    struct block_header *block2_header = block_get_header(block2);
    struct block_header *block3_header = block_get_header(block3);
	if (block1_header->is_free == true || block2_header->is_free == false || block3_header->is_free == true) {
		fprintf(stderr, "test 2 failed \n");
        return 0;
	}
    _free(block3);
	_free(block2);
	_free(block1);
	fprintf(stdout, "freeing the heap: \n");
    debug_heap(stdout, HEAP_START);
	return 1;

}

