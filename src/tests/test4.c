#include "../tests.h"

#define MAX_SIZE 50000


//Память закончилась, новый регион памяти расширяет старый.
int test4(){
    fprintf(stdout, "Test №4 \n");
	void* block1 = _malloc(SIZE);
    debug_heap(stdout, HEAP_START);
    void* block2 = _malloc(MAX_SIZE);
    if (block1 == NULL || block2 == NULL ){
        fprintf(stderr, "test 4 failed \n");
        return 0;
    }

	debug_heap(stdout, HEAP_START);

	struct block_header *block1_header = block_get_header(block1);
    struct block_header *block2_header = block_get_header(block2);
    if (block1_header->next != block2_header) {
        fprintf(stderr, "test 4 failed \n");
        return 0;
    }
	_free(block2);
	_free(block1);
	fprintf(stdout, "freeing the heap: \n");
    debug_heap(stdout, HEAP_START);
	return 1;
}

