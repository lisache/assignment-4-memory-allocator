#include "../tests.h"

//Обычное успешное выделение памяти.
int test1() {
    fprintf(stdout, "Test №1 \n");
    void *block = _malloc(SIZE);
    struct block_header *block1_header = block_get_header(block);
    if (block == NULL || block1_header->capacity.bytes != SIZE || block1_header->is_free == false){
        fprintf(stderr, "test 1 failed \n");
        return 0;
    }
    debug_heap(stdout, HEAP_START);
    _free(block);
    fprintf(stderr, "freeing the heap: \n");
    debug_heap(stderr, HEAP_START);
    return 1;
}
