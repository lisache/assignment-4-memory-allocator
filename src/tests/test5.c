#include "../tests.h"


//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
int test5(){
    fprintf(stdout, "Test №5 \n");
	void* block1 = _malloc(SIZE);
    struct block_header *block1_header = block_get_header(block1);


    debug_heap(stdout, HEAP_START);

    void *after_heap = (void *) block1_header->contents + block1_header->capacity.bytes;
    void* mapped_pg = map_pages(after_heap, SIZE, MAP_PRIVATE | MAP_FIXED);


    void* block2 = _malloc(SIZE);
    if (block1 == NULL || block2 == NULL ){
        fprintf(stderr, "test 4 failed \n");
        return 0;
    }

	debug_heap(stdout, HEAP_START);

	
    struct block_header *block2_header = block_get_header(block2);
    if (block1_header->next != block2_header) {
        fprintf(stderr, "test 4 failed \n");
        return 0;
    }
	_free(block2);
	_free(block1);
	fprintf(stdout, "freeing the heap: \n");
    debug_heap(stdout, HEAP_START);
    munmap(mapped_pg, REGION_MIN_SIZE);
	return 1;
}

