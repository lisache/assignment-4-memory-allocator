#ifndef _TESTS_H
#define _TESTS_H

#define SIZE 1000
#define TESTS_NUM 5
#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/mman.h>
int test1();
int test2();
int test3();
int test4();
int test5();
int check();
#endif //_TESTS_H
